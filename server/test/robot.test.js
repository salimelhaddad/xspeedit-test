import Robot from "../models/robot.js";

describe("Robot", () => {
  let robot;

  beforeEach(() => {
    // @note: nous instancions Robot avant chaque test:
    robot = new Robot();
  });

  afterEach(() => {
    // @note: nous nettoyons notre instance après chaque test:
    robot = null;
  });

  it("should set input correctly", () => {
    // GIVEN: un input respectant les contraintes:
    const input = "1234";
    // WHEN: nous affectons l'input:
    robot.setInput(input);
    // THEN: la propriété input doit être affecté correctement:
    expect(robot.input).toEqual([1, 2, 3, 4]);
  });

  it("should not set input when it doesn't match constraints", () => {
    // GIVEN: un input ne respectant pas les contraintes:
    const input = "0/1234";
    // WHEN: nous affectons l'input:
    const thrower = () => robot.setInput(input);
    // THEN: une erreur doit être capturée:
    expect(thrower).toThrowError(/Saisie invalide: vous devez saisir un nombre/);
  });

  it("should set algorithm correctly", () => {
    // GIVEN: une strategie respectant les contraintes sur sa signature de fonction:
    const expectedOutput = () => {};
    const algorithm = () => expectedOutput;
    // WHEN: nous affectons la algorithm:
    robot.setInput("1234");
    robot.setStrategy(algorithm);
    // THEN: la propriété algorithm doit être affecté correctement:
    expect(robot.algorithm).toBe(expectedOutput);
  });

  it("should not set algorithm when input is not valid", () => {
    // GIVEN: une strategie respectant les contraintes sur sa signature de fonction:
    const expectedOutput = () => {};
    const algorithm = () => expectedOutput;
    // WHEN: nous affectons la algorithm sans avoir affecté d'input (eg. null):
    const thrower = () => robot.setStrategy(algorithm);
    // THEN: une erreur doit être capturée:
    expect(thrower).toThrowError(
      /Vous devez effectuer une saisie avant de pouvoir affecter une stratégie/
    );
  });

  it("should not set algorithm when it doesn't match function signature", () => {
    // GIVEN: une strategie ne respectant pas les contraintes sur sa signature de fonction:
    const algorithm = "toto";
    // WHEN: nous affectons la algorithm:
    robot.setInput("1234");
    const thrower = () => robot.setStrategy(algorithm);
    // THEN: une erreur doit être capturée:
    expect(thrower).toThrowError(/is not a function/);
  });

  it("should resolve correctly", () => {
    // GIVEN: une strategie ne respectant pas les contraintes sur sa signature de fonction:
    const algorithm = () => () => ({ boxes: [[1, 2], [3, 4]] });
    // WHEN: nous affectons la algorithm:
    robot.setInput("1234");
    robot.setStrategy(algorithm);
    // THEN: une erreur doit être capturée:
    expect(robot.resolve()).toEqual({ raw: [[1, 2], [3, 4]], formatted: "12/34" });
  });

  it("should set capacity correctly", () => {
    // GIVEN: une capacité:
    const capacity = 25;
    // WHEN: nous affectons la capacité à l'instance Robot:
    robot.capacity = capacity;
    // THEN: la propriété capacity a bien été affectée:
    expect(robot.capacity).toBe(25);
  });

  describe("contructor", () => {
    it("should instanciate correctly without argument", () => {
      // GIVEN: une instance robot:
      const robot = new Robot();
      // THEN: les propriétés de l'instance doivent être affectés correctement:
      expect(robot.capacity).toBe(10);
      expect(robot.input).toBe(null);
    });

    it("should instanciate correctly with argument", () => {
      // GIVEN: une instance robot:
      const robot = new Robot(25);
      // THEN: les propriétés de l'instance doivent être affectés correctement:
      expect(robot.capacity).toBe(25);
      expect(robot.input).toBe(null);
    });
  });
});
