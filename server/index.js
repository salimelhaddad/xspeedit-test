// import Robot, { optimizedAlgorithm, unoptimizedAlgorithm } from "xspeedit-core";
import Robot from "./models/robot";
import optimizedAlgorithm from "./core-algorithm/optimized";
import unoptimizedAlgorithm from "./core-algorithm/unoptimized"
import readline from "readline";

// @note: nous instancions notre robot empaqueteur:
const robot = new Robot(10);
// @note: nous créons l'interface de lecture de l'input (sur buffer STDIN):
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// @note: nous attendons la saisie utilisateur via question() readline api:
rl.question("Articles      : ", answer => {
  const { raw: rawOptiBoxes, formatted: optiOutput } = robot
    .setInput(answer)
    .setStrategy(optimizedAlgorithm)
    .resolve();
  const { raw: rawUnoptiBoxes, formatted: unoptiOutput } = robot
    .setInput(answer)
    .setStrategy(unoptimizedAlgorithm)
    .resolve();

  console.log(`Robot actuel  : ${unoptiOutput} => ${rawUnoptiBoxes.length} cartons utilisés`);
  console.log(`Robot optimisé: ${optiOutput} => ${rawOptiBoxes.length} cartons utilisés`);

  // @note: nous fermons et nettoyons notre interface:
  rl.close();
});
