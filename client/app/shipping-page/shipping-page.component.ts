import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastComponent} from '../shared/toast/toast.component';
import {Box} from '../shared/models/box';
import {ShippingService} from '../services/shipping.service';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-shipping-page',
  templateUrl: './shipping-page.component.html',
  styleUrls: ['./shipping-page.component.scss']
})
export class ShippingPageComponent implements OnInit {

  box = new Box();
  shippedArticles: any;
  optimizedBoxes: any[];
  unoptimizedBoxes: any[];



  articlesForm: FormGroup;
  articles = new FormControl('', Validators.required);
  capacity = new FormControl('', Validators.required);
  error: string;



  constructor(private shippingService: ShippingService,
              private formBuilder: FormBuilder,
              public toast: ToastComponent) { }

  ngOnInit() {
    this.articlesForm = this.formBuilder.group({
      articles: this.articles,
      capacity: this.capacity

    });
  }

  onSubmit() {
    this.box.articles = this.articles.value;
    this.box.capacity = (this.capacity.value) ? this.capacity.value : 10;
    this.shippingService.shipArticles(this.box).subscribe(
      (data) => {
        this.shippedArticles = data;
        if (data.error) {
          this.toast.setMessage(data.error, 'danger');
          this.error = data.error;
        } else {
          this.unoptimizedBoxes = data.unoptiOutput.split('/').filter((e) => {
            return e !== '';
          });

          this.optimizedBoxes = data.optiOutput.split('/').filter((e) => {
            return e !== '';
          });
          this.toast.setMessage('articles shipped successfully.', 'success');
        }


      },
      (error) => {
        this.error = error;

      }

    );


  }








}
