# [Lien Gitlab](https://gitlab.com/salimelhaddad/xspeedit-test) : Le code is hosted in gitlab. CI/DI is triggered by Gitlab-ci

# Documentation fonctionnelle

## Contexte

XspeedIt est une société d'import / export ayant robotisé toute sa chaîne d'emballage de colis.
Elle souhaite trouver un algorithme permettant à ses robots d'optimiser le nombre de cartons d'emballage utilisés.

Les articles à emballer sont de taille variable, représentée par un entier compris entre 1 et 9.
Chaque carton a une capacité de contenance de 10.
Ainsi, un carton peut par exemple contenir un article de taille 3, un article de taille 1, et un article de taille 6.

La chaîne d'articles à emballer est représentée par une suite de chiffres, chacun représentant un article par sa taille.
Après traitement par le robot d'emballage, la chaîne est séparée par des "/" pour représenter les articles contenus dans un carton.

*Exemple*
```python
Chaîne d'articles en entrée : 163841689525773
Chaîne d'articles emballés  : 163/8/41/6/8/9/52/5/7/73
```

L'algorithme actuel du robot d'emballage est très basique.
Il prend les articles les uns après les autres, et les mets dans un carton.
Si la taille totale dépasse la contenance du carton, le robot met l'article dans le carton suivant.

# Documentation technique

## Introduction

It is a mean stack project build with node.js, express and Angular 7

The folder , **xspeedit/server/core-algorithm**, contains the algorithms used to ship boxes with artices



* **xspeedit IHM** ([Accessible ici](https://xspeedit.herokuapp.com/ship)): Angular application. 
it exposes an interface to set the articles for shipping.
	

<p align="center">
	<img width="1000"  src="https://gitlab.com/salimelhaddad/xspeedit-test/raw/ac90e42d4fb9ef7c3599d773fc9a192f74212b73/shipping.PNG"
	alt="screenshot" />
</p>

* **xspeedit console**: application backend Node.js:

<p align="center">
	<img width="1000" src="https://gitlab.com/salimelhaddad/xspeedit-test/raw/master/console-node.PNG"
	alt="screenshot" />
</p>

The frontend is generated with [Angular CLI](https://github.com/angular/angular-cli). The backend is made from scratch using node.js/express.

This project uses the [MEAN stack](https://en.wikipedia.org/wiki/MEAN_(software_bundle)):
* [**E**xpress.js](http://expressjs.com): backend framework
* [**A**ngular 7](https://angular.io): frontend framework
* [**N**ode.js](https://nodejs.org): runtime environment

Other tools and technologies used:
* [Angular CLI](https://cli.angular.io): frontend scaffolding
* [Bootstrap](http://www.getbootstrap.com): layout and styles
* [jasmine](https://angular.io/guide/testing): unit testing for front-end
* [jest](https://jestjs.io/): unit testing for back-end


# CI/CD using Gitab
## Description
CI/CD is build using [.gitlab-ci.yml](https://gitlab.com/salimelhaddad/xspeedit-test/blob/master/.gitlab-ci.yml) file. It creates a pipelines triggered once the code 
is pushed to the repository : 



The pipeline is composed of three stages : 
1. unit tests : The unit tests front and back are executed concurrently.
2. coverage tests : the coverage test front and back are executed concurrently.
3. production : the application is deployed in Heroku.[CLick here to preview](https://xspeedit.herokuapp.com/ship)

## Preview 
[CLick here](https://gitlab.com/salimelhaddad/xspeedit-test/pipelines) the pipelines after execution.


## Prerequisites
1. Install [Node.js](https://nodejs.org) 
2. Install Angular CLI: `npm i -g @angular/cli`
3. From project root folder install all the dependencies: `npm i`

## Run
### Run in console : Without IHM
`npm run console`: [babel](https://babeljs.io/en/setup), [nodemon](https://nodemon.io/).

### Development mode
`npm run dev`: [concurrently](https://github.com/kimmobrunfeldt/concurrently), Angular build, TypeScript compiler and Express server.

A window will automatically open at [localhost:4200](http://localhost:4200). Angular and Express files are being watched. Any change automatically creates a new bundle, restart Express server and reload your browser.

### Production mode
`npm run prod`: run the project with a production bundle and AOT compilation listening at [localhost:3000](http://localhost:3000) 

## Deploy (Heroku)
1. Go to Heroku and create a new app (eg: `your-app-name`)
2. Install [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)
3. `heroku login`
4. `mkdir your-app-name && cd your-app-name`
5. `git init`
6. `heroku git:remote -a your-app-name`
7. Download this repo and copy all files into `your-app-name` folder
8. `npm i`
9. Edit `package.json` as following:
   - add this line to scripts: `"postinstall": "tsc -p server && ng build --aot --prod"`
   - move the following packages from devDependencies to dependencies: `@angular/cli`, `@angular/compiler-cli`, `@types/*` and `typescript`.
10. `git add .`
11. `git commit -m "Going to Heroku"`
12. `git push heroku master`
13. `heroku open` and a window will open with your app online

## Demo
[CLick here](https://xspeedit.herokuapp.com/ship)

## Running tests back
Run `npm run testbe` to execute the backend unit tests via  [jest](https://jestjs.io/).

Run `npm run test:coverage-back` to execute coverage tests via [jest](https://jestjs.io/). To see coverage results, open the file **xspeedit/coverage/lcov-report/index.html** in your browser.  


## Running tests front
Run `npm run test` to execute the frontend unit tests via  [Karma](https://karma-runner.github.io),[PhantomJS](http://phantomjs.org/download.html) and [jasmine](https://angular.io/guide/testing) .

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Run `npm run test:coverage-front` to execute coverage tests via [Istanbul](https://www.npmjs.com/package/karma-istanbul). To see coverage results, open the file **xspeedit/coverage/index.html** in your browser



## Running linters
Run `ng lint` to execute the frontend TS linting via [TSLint](https://github.com/palantir/tslint).


Run `npm run linthtml` to execute the frontend HTML linting via [HTMLHint](https://github.com/htmlhint/HTMLHint).

Run `npm run lintscss` to execute the frontend SCSS linting via [SASS-Lint](https://github.com/sasstools/sass-lint).



#

### Author
* [Salim EL HADDAD](https://github.com/salimelhaddad)
