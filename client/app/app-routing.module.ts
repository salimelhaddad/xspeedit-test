// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Services

// Components
import {ShippingPageComponent} from './shipping-page/shipping-page.component';


const routes: Routes = [
  { path: 'ship', component: ShippingPageComponent },

  { path: '**', redirectTo: '/ship' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
