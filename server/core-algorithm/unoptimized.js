import Box from "../models/box";

/**
 * Fonction définissant l'algorithme non optimisé d'empaquetage des articles dans les cartons
 * @function
 * @name unoptimizedAlgorithm
 * @param {Object} 		data { articles: La valeur de la propriété input décrivant les articles suivant leur taille, capacity: La contenance maximale d'une boîte }
 * @return {Function}	Le callback permettant de lancer la résolution
 */
export default ({ articles, capacity }) => {


  return () => {
    // @note: contient la liste des cartons générés:
    let boxes = [];

    let box = new Box(capacity);
    articles.forEach(article => {
      // @note: le carton en cours ne peut plus ajouter d'article
      if (!box.add(article)) {
        boxes = [...boxes, box.articles];
        box = new Box(capacity, article);
      }
    });
    boxes = [...boxes, box.articles];

    return { boxes };
  };
};
