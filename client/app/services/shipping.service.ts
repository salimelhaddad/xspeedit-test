import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Box} from '../shared/models/box';

@Injectable({
  providedIn: 'root'
})
export class ShippingService {

  constructor(private httpClient: HttpClient) { }

  shipArticles(box: Box): Observable<any> {
    return this.httpClient.post<Box>('/ship', box);
  }
}
