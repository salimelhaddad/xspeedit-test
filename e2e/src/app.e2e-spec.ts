import { browser } from 'protractor';
import { Angular2FullStackPage } from './app.po';

describe('xspeedit', () => {
  let page: Angular2FullStackPage;

  beforeEach(() => {
    page = new Angular2FullStackPage();
  });

  it('should true to be true', () => {
    browser.manage().window().setSize(1024, 768);
    page.navigateTo();

    expect(true).toBeTruthy();
  });

  it('should works', () => {
    browser.manage().window().setSize(640, 480);
    page.navigateTo();
    expect('').toEqual('');
  });
});
