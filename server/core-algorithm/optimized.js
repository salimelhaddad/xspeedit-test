import Box from "../models/box";
import { compareAsc } from "../utilities/algorithm-helper.js";

/**
 * Fonction définissant l'algorithme optimisé d'empaquetage des articles dans les cartons
 * @function
 * @name optimizedAlgorithm
 * @param {Object} 		data { articles: La valeur de la propriété input décrivant les articles suivant leur taille, capacity: La contenance maximale d'une boîte }
 * @return {Function}	Le callback permettant de lancer la résolution
 */
export default ({ articles, capacity }) => {

  let currentNode;
  let bestNode;

  const permuteNode = () => {
    bestNode = currentNode;
  };

  const findSolution = (article, ...subdomain) => {
    // @note: taille === 10 <=> on ne peut pas trouver de meilleur candidat
    // (on retourne dans ce cas, le meilleur noeud candidat):
    if (bestNode.size === capacity || subdomain.length === 0) {
      return bestNode.articles;
    }

    currentNode = new Box(capacity, article);

    for (let i = 0; i < subdomain.length; i++) {
      const candidateArticle = subdomain[i];

      currentNode.add(candidateArticle);
      if (currentNode.size > bestNode.size) {
        permuteNode();
      }
    }

    // eslint-disable-next-line no-unused-vars
    const [excludeCandidate, ...newSubdomain] = subdomain;

    return findSolution(article, ...newSubdomain);
  };

  return () => {
    let boxes = [];
    // @note:  [...articles].sort(compareAsc) comme valeur initialenous trions
    // nos articles par taille décroissante pour que notre bestNode
    // corresponde, à l'initialisation, à la borne supérieure:
    const sortedArticles = [...articles].sort(compareAsc);

    sortedArticles.reduce(remainingArticles => {
      if (remainingArticles.length === 0) {
        //@note: plus de candidats
        return [];
      }

      const [candidateArticle, ...others] = remainingArticles;
      // @note: nous définissons la borne supérieure avant d'effectuer la recherche de
      // solution (correspondant à l'article (triée par ordre décroissant dans articles)):
      bestNode = new Box(capacity, candidateArticle);
      const solution = findSolution(candidateArticle, ...others);
      boxes = [...boxes, solution];

      const nextRemainingArticles = [...remainingArticles];
      solution.forEach(entry => {
        nextRemainingArticles.splice(nextRemainingArticles.indexOf(entry), 1);
      });

      // @note: retourne les sous-domaines de recherche restants:
      return nextRemainingArticles;
    }, sortedArticles);

    return { boxes };
  };
};
