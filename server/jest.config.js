module.exports = {
  // @note: Nous affichons les logs complets de test:
  verbose: true,


  projects: ["<rootDir>/*"],

  // @note: configurations de la couverture de test:
  collectCoverage: true,
  coverageDirectory: "./coverage",
  collectCoverageFrom: [
    "**/*.{js,jsx}",
    "!**/index.{js,jsx}",
    "!**/node_modules/**",
    "!**/*.{ts,json}",
    "!**/doc/**",
    "!**/dist/**",
    "!**/*.config.js",
    "!**/*.setup.js"
  ],

  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: 80
    }
  }
};
