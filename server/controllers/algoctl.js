import Robot from "../models/robot";

import optimizedAlgorithm from "../core-algorithm/optimized";
import unoptimizedAlgorithm from "../core-algorithm/unoptimized";
export default class AlgoCtl {
  // @note: nous instancions notre robot empaqueteur:



  packageArticles = (req, res) => {
    // this.model.findOne({ email: req.body.email }, (err, user) => {
    //   if (!user) { return res.sendStatus(403); }
    //   user.comparePassword(req.body.password, (error, isMatch) => {
    //     if (!isMatch) { return res.sendStatus(403); }
    //     const token = jwt.sign({ user: user }, process.env.SECRET_TOKEN); // , { expiresIn: 10 } seconds
    //     res.status(200).json({ token: token });
    //   });
    // });
    const robot = new Robot(req.body.capacity);
    let response = null;
    try{
      console.log(req.body);
      const { raw: rawOptiBoxes, formatted: optiOutput } = robot
        .setInput(req.body.articles)
        .setStrategy(optimizedAlgorithm)
        .resolve();
      const { raw: rawUnoptiBoxes, formatted: unoptiOutput } = robot
        .setInput(req.body.articles)
        .setStrategy(unoptimizedAlgorithm)
        .resolve();
      console.log(`Robot actuel  : ${unoptiOutput} => ${rawUnoptiBoxes.length} cartons utilisés`);
      console.log(`Robot optimisé: ${optiOutput} => ${rawOptiBoxes.length} cartons utilisés`);
      response = {
        unoptiOutput : unoptiOutput,
        optiOutput : optiOutput
      };

    }catch (e) {

      response = {
        error : e.message
      };

    }finally {
      res.json(response);
    }
  }


}
