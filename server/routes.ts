import * as express from 'express';
import AlgorithmCtl from './controllers/algoctl';



export default function setRoutes(app) {

  const router = express.Router();
  const algoCtl = new AlgorithmCtl();



  router.route('/ship').post(algoCtl.packageArticles);

  // Apply the routes to our application with the prefix /api
  app.use('/', router);

}
