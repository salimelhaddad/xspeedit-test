import unoptimizedStrategy from "../core-algorithm/unoptimized.js";

describe("UnoptimizedStrategy", () => {
  const data = { articles: [5,8,9,1,4,3,6,2,7,8], capacity: 10 };

  it("should apply unoptimized algorithm correctly", () => {
    // GIVEN: une strategy avec un packager
    const strategy = unoptimizedStrategy(data);
    // WHEN: nous exécutons notre strategy:
    const response = strategy();
    // THEN: nous vérifions quel'algorithme s'applique correctement:
    expect(response).toEqual({
      boxes: [[5], [8], [9,1], [4,3], [6,2], [7], [8]]
    });
    expect(response.boxes).toHaveLength(7);
  });
});
