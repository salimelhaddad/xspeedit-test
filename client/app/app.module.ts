// Angular
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// Modules
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
// Services

// Components
import { AppComponent } from './app.component';
import { ShippingPageComponent } from './shipping-page/shipping-page.component';
import {ShippingService} from './services/shipping.service';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';




@NgModule({
  declarations: [
    AppComponent,
    ShippingPageComponent,

  ],
  imports: [
    AppRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule

  ],
  providers: [

    ShippingService

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})

export class AppModule { }
